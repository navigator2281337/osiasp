﻿using Osi.Domain;
using OSI.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenerateData
{
   public  class NewGen
    {

        private readonly IOSIContext _context;

        public NewGen()
        {
        }

        public NewGen(IOSIContext context)
        {
            _context = context;
        }

        public void Gen(CancellationToken cancellationToken)
        {
            for (int city = 1; city <= 10; city++)
            {
                _context.Cities.Add(new City() {Id = city, Name = "Город" + city});

                    for (int street = 1; street <= 250; street++)
                    {
                    _context.Streets.Add(new Street { Id = street, Name = "Улица" + street, CityId = city});
                    }  
            }

            
            _context.SaveChangesAsync(cancellationToken);


        }

     
    }
}
