﻿using AutoMapper;
using Osi.Domain;
using OSI.Application.Logic.House.Command.Create;
using OSI.Application.Logic.RangeFlatOsi.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Application.Mapping
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<House, CreateHouseCommand>().ReverseMap();
            CreateMap<CreateHouseCommand, House>().ReverseMap();    
            
            CreateMap<RangeFlatOsi, CreateRangeFlatOsiCommand>().ReverseMap();
            CreateMap<CreateRangeFlatOsiCommand, RangeFlatOsi>().ReverseMap();
        }
    }
}
