﻿using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Interfaces
{
    public interface IOSIContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Osi.Domain.Osi> Osi{ get; set; }
        public DbSet<RangeFlatOsi> RangeFlatOsi { get; set; }
        public DbSet<Flat> Flat { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
