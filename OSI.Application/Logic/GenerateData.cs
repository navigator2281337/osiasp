﻿using Osi.Domain;
using OSI.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic
{
    public class GenerateData
    {
        private readonly IOSIContext _context;

        public GenerateData()
        {
        }

        public GenerateData(IOSIContext context)
        {
            _context = context;
        }

        public void Gen(CancellationToken cancellationToken)
        {


            int num = 1;
            const int  maxSaveString = 1000;
            DateTime timeStart = DateTime.Now;
            for (int city = 1; city <= 10; city++)
            {
                _context.Cities.Add(new City() { Id = city, Name = "Город" + city });

                for (int street = 1; street <= 250; street++)
                {
                    _context.Streets.Add(new Street { Id = street, Name = "Улица" + street, CityId = city });

                    for (int house = 1; house <= 20; house++)
                    {
                        _context.Houses.Add(new Osi.Domain.House { Id = house, Number = house, StreetId = street });

                        for (int flat = 1; flat <= 40; flat++)
                        {
                            _context.Flat.Add(new Flat { Id = flat, Number = flat, HouseId = house });
                            num++;
                            if (num == maxSaveString)
                            {
                                _context.SaveChangesAsync(cancellationToken);
                            }
                        }
                    }
                }
            }


            for (int osi = 1; osi <= 500; osi++)
            {
                _context.Osi.Add(new Osi.Domain.Osi { Id = osi, Name = "УК"+osi });
            }
            _context.SaveChangesAsync(cancellationToken);


            DateTime timeEnd = DateTime.Now;

            Console.WriteLine(timeEnd - timeStart);
        }


        public void Svyaz(CancellationToken cancellationToken)
        {
            Random rand = new Random();
            for (int city = 1; city <= 10; city++)
            {
                for (int street = 1; street <= 250; street++)
                {
                    for (int house = 1; house <= 20; house++)
                    {
                        for (int flat = 1; flat <= 40; flat++)
                        {
                            _context.RangeFlatOsi.Add(new Osi.Domain.RangeFlatOsi
                            {
                                OsiId = rand.Next(0, 500),
                                HouseId = house,

                                From = 1,
                                To = 40
                            });
                        }
                    }
                }
            }
        }


       
    }
}
