﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OSI.Application.Exceptions;
using OSI.Application.Interfaces;
using OSI.Application.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OSI.Application.Logic.RangeFlatOsi.Command
{
    public class CreateRangeFlatOsiCommand : IRequest<Response<int>>
    {
        public int OsiId { get; set; }
        public int HouseId { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }


    public class CreateRangeFlatOsiCommandHandler : IRequestHandler<CreateRangeFlatOsiCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IOSIContext _context;

        public CreateRangeFlatOsiCommandHandler(IMapper mapper, IOSIContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Response<int>> Handle(CreateRangeFlatOsiCommand request, CancellationToken cancellationToken)
        {

            var isExist = await _context.RangeFlatOsi.AnyAsync(u => u.OsiId == request.OsiId
            && u.HouseId == request.HouseId
            && u.From == request.From
            && u.To == request.To);

            var rangeFlatOsi = _mapper.Map<Osi.Domain.RangeFlatOsi>(request);

            if (!isExist)
            {
                _context.RangeFlatOsi.Add(rangeFlatOsi);
                var result = await _context.SaveChangesAsync(CancellationToken.None);
                return new Response<int>(result);
            }
            else
                throw new ApiException($"{rangeFlatOsi} not create");
        }
    }

}
