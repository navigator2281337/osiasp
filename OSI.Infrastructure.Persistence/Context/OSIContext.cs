﻿using Microsoft.EntityFrameworkCore;
using Osi.Domain;
using OSI.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSI.Infrastructure.Persistence.Context
{
    public class OSIContext : DbContext, IOSIContext
    {
        public OSIContext()
        {
        }

        public OSIContext(DbContextOptions<OSIContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RangeFlatOsi>()
              .HasNoKey();
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Osi.Domain.Osi> Osi { get; set; }  
        public DbSet<RangeFlatOsi> RangeFlatOsi { get; set; }
        public DbSet<Flat> Flat { get; set; }

    }
}
