﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osi.Domain
{
    public class Flat
    {
        public int Id { get; set; }
        public int Number { get; set; }

        public int HouseId { get; set; }
        public House House { get; set; }
    }
}
