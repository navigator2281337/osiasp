﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osi.Domain
{
    public class RangeFlatOsi
    {
        public int OsiId { get; set; }
        public Osi Osi { get; set; }

        public int HouseId { get; set; }
        public House House { get; set; }

        public int From { get; set; }
        public int To { get; set; }
    }
}
